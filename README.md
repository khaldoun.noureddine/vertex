##Vertex Task

I hope you find the task to your satisfaction, I set a two-hour alarm for myself to make sure
I don't go overtime. I wanted to do the website in plain PHP and the scheduler in Laravel to show
my skill level in both.
However, I decided to use only Laravel for everything so that I don't make starting the application
too annoying and to save time.

I am using Laravel 8 (you can change it in composer if you want)

Please find some more information below:

###Running the Application

To install this application, follow the same steps you would do when cloning any Laravel application 
( run ```composer install```, create database and modify .env file, generate app key, and migrate the database). However, make sure
that you run the following two commands: <br>
1. ```php artisan schedule:work```
2. ```php artisan queue:work```

Now you can use the application normally.

###Things I didn't finish in the 2 hr time.

1. Tests for the application: I did the research on how to test jobs, but I ran out of time.
2. As you noticed, you have to run the scheduler and queue worker manually. This could
have been solved by dockerizing everything and running cron jobs inside the container.
   However, I discovered that a bit late and wasn't able to do it in the allocated time.
   
