<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequestStoreRequest;
use App\Models\UserRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class UserRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): Application|Factory|View
    {
        $userRequests = UserRequest::paginate(10);
        return view('index', compact('userRequests'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequestStoreRequest $request
     * @return RedirectResponse
     */
    public function store(UserRequestStoreRequest $request): RedirectResponse
    {
        $userRequest = new UserRequest();
        $userRequest->fill($request->all());
        $userRequest->save();

        return back()->with('success','Request Successfully Added.');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id): View|Factory|Application
    {
        $userRequest = UserRequest::find($id);
        return view('show', ['userRequest' => $userRequest]);
    }
}
