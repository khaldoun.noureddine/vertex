<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['App\Http\Controllers\UserRequestController', 'index'])->name('home');
Route::post('/requests/store', ['App\Http\Controllers\UserRequestController', 'store'])->name('request.store');
Route::get('/requests/{id}', ['App\Http\Controllers\UserRequestController', 'show'])->name('request.show');

